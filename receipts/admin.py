from django.contrib import admin

# Register your models here.
from .models import ExpenseCategory, Account, Receipt


@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
    )


@admin.register(Account)
class AccountCategoryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
    )


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        "id",
    )
