from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from .forms import ReceiptForm, CategoryForm, AccountForm
# Create your views here.


@login_required
def receipt_list(request):
    user = request.user
    receipts = Receipt.objects.filter(purchaser=user)
    context = {
        "receipt_list": receipts,
    }
    return render(request, "receipts/list.html", context)


@login_required
def category_list(request):
    user = request.user
    category = ExpenseCategory.objects.filter(owner=user)
    context = {
        "category_list": category,
    }
    return render(request, "receipts/categories.html", context)


@login_required
def account_list(request):
    user = request.user
    account = Account.objects.filter(owner=user)
    context = {
        "account_list": account,
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form
    }
    return render(request, "receipts/create.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("category_list")
    else:
        form = CategoryForm()
    context = {
        "form": form
    }
    return render(request, "receipts/createcategory.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form
    }
    return render(request, "receipts/createaccount.html", context)
